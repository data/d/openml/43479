# OpenML dataset: USA-Airport-Dataset

https://www.openml.org/d/43479

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

What is it ?
This dataset is a record of 3.5 Million+ US Domestic Flights  from 1990 to 2009. It has been taken from OpenFlights website which have a huge database of different travelling mediums across the globe. I came across this dataset while I was preparing for a hackathon and thought it should on kaggle's dataset list.
What's in it ?
Here is some info about the attributes present in the dataset:

Origin_airport: Three letter airport code of the origin airport
Destination_airport: Three letter airport code of the destination airport
Origin_city: Origin city name
Destination_city: Destination city name
Passengers: Number of passengers transported from origin to destination
Seats: Number of seats available on flights from origin to destination
Flights: Number of flights between origin and destination (multiple records for one month, many with flights  1)
Distance:  Distance (to nearest mile) flown between origin and destination
Fly_date: The date (yyyymm) of flight
Origin_population: Origin city's population as reported by US Census
Destination_population: Destination city's population as reported by US Census

Where did you get it ?
I would like to thank the original author of this dataset Jacob Perkins for putting such a good effort and collecting this data. 

I will be updating this dataset for attributes that might reveal some more information about this dataset.
Thanks for stopping by.
Peace.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43479) of an [OpenML dataset](https://www.openml.org/d/43479). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43479/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43479/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43479/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

